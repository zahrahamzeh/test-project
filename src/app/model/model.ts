export interface toDo {
  title: string,
  priority: number
  isDone: boolean,
  pinned: boolean,
  color: string
}
