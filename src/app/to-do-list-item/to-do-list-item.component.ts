import { Component, EventEmitter, Input, Output } from '@angular/core';
import { toDo } from '../model/model';

@Component({
  selector: 'app-to-do-list-item',
  templateUrl: './to-do-list-item.component.html',
  styleUrls: ['./to-do-list-item.component.css']
})

export class ToDoListItemComponent {
  @Input() public item: toDo = {
    title: '',
    priority: 0,
    isDone: false,
    pinned: false,
    color: 'green'
  };
  @Output() public pin = new EventEmitter();
  @Output() public done = new EventEmitter();
  priorityLabels = ['low', 'medium' ,'high']

  pinTodo(){
    this.pin.emit(this.item.pinned);
  }

  doneTodo(){
    this.done.emit(this.item.isDone);
  }
}
