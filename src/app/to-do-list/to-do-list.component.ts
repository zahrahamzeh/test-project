import { Component } from '@angular/core';
import { toDo } from '../model/model';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent {
  priorities = [0, 1, 2];
  priorityLabels = ['low', 'medium', 'high'];
  colors = ['red', 'green', 'yellow', 'blue'];
  list: toDo[] = [
    {
      title: 'shopping',
      priority: 1,
      isDone: false,
      pinned: false,
      color: 'green'
    }
  ];
  filteredList: toDo[] = this.list;
  doneList: toDo[] = [];
  newItem: toDo = {
    title: '',
    priority: 0,
    isDone: false,
    pinned: false,
    color: 'green'
  };
  colorFilter: string = '';

  addNewItem() {
    if (this.newItem.title) {
      this.list.push({...this.newItem});
      this.sortList();
    }
  }

  sortList() {
    this.filteredList.sort((a, b) => {
      if (b.pinned || a.pinned) {
        return 0;
      }
      return b.priority - a.priority;
    });
  }

  filterByColor() {
    if (this.colorFilter) {
      this.filteredList = this.list.filter(item => item.color === this.colorFilter);
    } else {
      this.filteredList = this.list;
    }
    this.sortList();
  }

  pinTodo(pinned: boolean, index: number) {
    if (!pinned) {
      this.filteredList[index].pinned = true;
      const todo = this.filteredList[index];
      this.filteredList.splice(index, 1);
      this.filteredList.unshift(todo);
    } else {
      this.filteredList[index].pinned = false;
      this.sortList();
    }
  }

  doneTodo(done: boolean, index: number) {
    if (!done) {
      const todo = this.filteredList[index];
      this.filteredList.splice(index, 1);
      this.list.splice(this.list.findIndex(item=> item.title === todo.title), 1);
      this.doneList.push(todo);
    } else {
      this.doneList[index].isDone = false;
      this.list.push(this.doneList[index]);
      this.doneList.splice(index, 1);
      this.sortList();
    }
  }
}
